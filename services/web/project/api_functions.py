import pandas as pd
from .preprocessing import batchify_docs, Corpus
from .keyword_extraction_main import predict


def extract_keywords(text, model, dictionary, sp, args):
    all_docs = [[1, text]]
    df_test = pd.DataFrame(all_docs)
    df_test.columns = ["id", "text"]
    corpus = Corpus(df_test, dictionary, sp, args)
    test_data = batchify_docs(corpus.test, 1)
    model.eval()
    predictions = predict(test_data, model, sp, corpus, args)
    return predictions[:args['kw_cut']]
                                   